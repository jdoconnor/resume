require 'grape'

module ResumeApi
  class API < Grape::API
    format :json

    desc "Get resume information"
  	get do
  	  resume = Resume.new
  	  resume.serialized_hash
  	end

  end
end
