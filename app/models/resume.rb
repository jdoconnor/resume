class Resume

	# the main hash that will be shown through the api.
  def serialized_hash
  	{
      contact: contact,
      works_with: works_with,
      experience: experience,
      education: education,
      misc: misc
  	}
  end

  private
    def contact
      {
        email: 'jay@jayoconnor.com',
        phone: '773.339.0270',
        websites: [ 'http://www.jayoconnor.com',
                    'https://github.com/jdoconnor',
                    'https://bitbucket.org/jdoconnor',
                    'http://www.linkedin.com/pub/jay-o-connor/2/316/170'
                  ]
      }
    end

  	def works_with
  		{
  			languages: ['Ruby', 'Javascript', 'CoffeeScript', 'Java', 'HTML', 'HAML', 'SCSS', 'c', 'c++'],
  			framworks_and_tools: ['Rails', 'Grape', 'RabbitMQ', 'Node.js', 'Sinatra', 'Android', 'Chef'],
  			data_stores: ['MySQL', 'MongoDB', 'Redis', 'Postgres', 'Memcache'],
  		}
  	end

  	def experience
  		[
  		  {
  		  	name: "Belly",
  		  	position: "Director of Engineering",
  		  	start_date: Time.new('2011', '12', '08'),
  				end_date: nil,
  		  	description: "Full stack development and architecture of a customer loyalty system, primarily focused on backend APIs and services and external service integration.  Scaled the system in Rails from around 100 businesses in Chicago to over 8000 nationwide while maintaining a focus on execution speed, code simplicity and maintainability through comprehensive testing and development best practices.  Designed and implemented a service oriented architecture (SOA) written in Grape to replace a single Rails application. Implemented email sending and tracking system incorporating variance testing. Created server software and interfaced with external partners to enable us being launch partners on Apple’s Passbook, Samsung’s Wallet, and Google’s Wallet.  Wrote and maintained Belly’s Facebook OpenGraph integration along with Facebook page specific tabs for merchants.  Wrote an API security library that enforces action limitations and data filtering based on user/client permissions while limiting database interaction to keep transaction speed at a minimum.",
          website: 'http://www.bellycard.com'
        },
  		  {
  		  	name: "Extendmed",
  		  	position: "Software/Systems Engineer/Strategist",
  		  	start_date: Time.new('2009', '03', '01'),
  				end_date: Time.new('2011', '12', '31'),
  		  	description: "Wrote and maintained applications for JBOSS (J2EE) Application Server for continuing medical education websites, using SQL, JSP, Hibernate, and Java.  Wrote and designed Actionscript driven audio, video, and image slide based presentations powered by Flash.  Migrated network and server infrastructure to a more mature model with J2EE clustering, load balancing, network access redundancy, and more efficient and complete data backups.  Implemented web application using Rails and Visual Basic to provide custom Powerpoint presentations that comply with legal regulations.  Developed prototypes of technical solutions and strategies for clients, such as creating custom learning management extensions to Joomla CMS.",
          website: 'http://www.extendmed.com'
  		  },
  		  {
  		  	name: "Transit Alarm",
  		  	position: "Creator",
  		  	description: "Created and wrote an Android Application targeted toward Chicago Transit bus commuters.  The application ingests time estimates from Chicago Transit Authority and allows users to set alarms directly on their device which inform them when a bus is X minutes away from a particular stop.  This application was written prior to any official APIs being available, so data was gathered using screen scraping and regular expressions.",
  		  },
  		  {
  		  	name: "Teamsubs",
  		  	position: "Founder",
  		  	start_date: Time.new('2008', '04', '01'),
  		  	description: "Wrote online RSVP system designed around recreational sports teams, implemented in Ruby On Rails.  Designed data architecture of a data driven website from conception.  Designed use cases of a customer facing website application.  Implemented all Ruby backend logic, including custom list management logic unique to the online RSVP space",
          website: 'http://www.teamsubs.com'
  		  },
  		  {
  		  	name: "Motorola.  Home And Network Mobility Technology Office",
  		  	position: "Sr Software Engineer",
  		  	start_date: Time.new('2006', '08', '01'),
  				end_date: Time.new('2008', '10', '31'),
  		  	description: "Developed, architected, and designed home media solutions. Worked on design and implementation of DLNA (a protocol built around XML web services) based media sharing with a focus on driving media to portable devices and set top boxes.  Primary focuses included porting and enhancing a Digtal Media Server (DMS) to a STB, architecture and software design of DLNA based applications and devices that can create a complaint ecosystem, user interface design, media transcoding, and integration of proprietary and open source solutions to minimize time to market and maximize compatibility.  Software is a combination of C++ and Java to operate within the OCAP specifications written for embedded Linux. Lead development teams by providing scheduling, architecture, design and code reviews for less experienced developers.  Submitted intellectual property regarding current work, new technologies, and possible upcoming projects.  Creating new applications for existing technology, and enhancing existing projects or products.  Prepared and presented prototypes and pre-product implementations to C level executives of potential and current customers."
  		  },
  		  {
  		  	name: "Motorola.  Wimax Base Station Software Group",
  		  	position: "Sr Software Engineer",
  		  	start_date: Time.new('2005', '06', '01'),
  				end_date: Time.new('2006', '9', '01'),
  		  	description: "Developer of the WiMax (802.16e) infrastructure network management components.  Setting up development and test environments, writing and reviewing architecture of components and system, and integrating services into our end solution.  Managed code and design work from external global software team, which included code, design, and system architecture reviews for a network management framework onto which a network management solution for the WiMax project utilized for software management, fault and message reporting, and system wide updates.  Software is built as a combination of handwritten C, C++, and generated C from UML in an Agile-like environment. "
  		  },
  		  {
  		  	name: "Motorola.  Telematics Software Group",
  		  	position: "Sr Software Engineer",
  		  	start_date: Time.new('2002', '05', '01'),
  				end_date: Time.new('2005', '6', '01'),
  		  	description: "Developer of embedded real time software and test tools for in-vehicle entertainment and safety systems. Design, implementation, validation and MOL of software components for RTXC based telematics platform which supported Bluetooth, diagnostics, power management, user input and indicator output. Design and implementation of new software components for Linux based telematics platform including porting of Sun Java Virtual Machine to proprietary PPC Linux based telematics platform. Setup and management of OSGi framework and services for use in a telematics environment. Software architecture and customer presentations for future OnStar systems. System and Software Validation for OnStar systems. Design and implementation of full system simulation for OnStar Systems, to assist development, testing, and automation within a lab environment. Functionality simulated in automotive bus data (Class2, CAN/GMLAN), OnStar Call Center, cellular network (CDMA, AMPS), power inputs, power consumption, and user inputs (voice and button). Design and implementation of full system simulation for Ford Bluetooth system, to assist development, testing, and automation within a lab environment. Functionality simulated in automotive bus data (CAN/FNOS). Testing and simulation frameworks written primarily in Visual Basic, and C++ utilizing COM communication between components. Training and mentoring others on OnStar system, lab tools and utilities, telematics in general.",
  		  },
  		  {
  		  	name: "Motorola.  GSM/iDEN/UMTS Base Station Software Group",
  		  	position: "Sr Software Engineer",
  		  	start_date: Time.new('1999', '12', '13'),
  				end_date: Time.new('2002', '5', '01'),
  		  	description: "Developer and troubleshooter in an S.E.I. Level 5 compliant environment.  Design and implementation of MMI and operations database for UMTS/GPRS/GSM cellular network components.  Design and implementation of software and hardware fault management systems for iDEN cellular network components.  Design and implementation of development tools, user tools, and integration of 3rd party software CodeTest and Flexelint.  Software written primarily in C, with tools being written in PERL, Visual Basic, and Rational Rose scripting language (similar to basic)"
				},

  		]
  	end

  	def education
  		[
  			'Post graduate courses toward MS in telecommunications from DePaul University.',
				'BS in Computer Science from DePaul University' ,
				'Associates in Arts degree from William Rainey Harper College',
				'Associates in Science degree from William Rainey Harper College'
  		]
  	end

  	def misc
  		[
  			"1 patent granted with USPTO, 4 patents pending, 1 filed internationally, numerous intellectual property submissions within Motorola and Belly",
				"Dean's List, DePaul University",
				"Dean's List, William Rainey Harper College",
				"Have performed at The Second City, Improv Olympic and The Annoyance Theater. Former faculty at The Second City."
  		]
  	end

end
