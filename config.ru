Bundler.require :default, ENV['RACK_ENV']

require "./app/models/resume.rb"
require "./app/apis/resume_api.rb"

run ResumeApi::API
